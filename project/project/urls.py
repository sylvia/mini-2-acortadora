
from django.contrib import admin
from django.urls import include, path
from django.shortcuts import redirect

urlpatterns = [
    path('', lambda request: redirect('/acorta/')),
    path('acorta/', include('acorta.urls')),
    path('admin/', admin.site.urls),
]

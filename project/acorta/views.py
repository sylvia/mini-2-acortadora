from django.shortcuts import render
from .models import Acortadora
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Max

# -- Importamos la clase Acortadora que creamos en el archivo models.py

def redirigir(request, short):
    # -- Cuando el recurso sea /<short> se ejecuta esta funcion.
    # -- Ahora vamos a buscar en la base de datos el short que se ha introducido.
    acortador = Acortadora.objects.get(short=short)
    # -- Ahora vamos a redirigir a la url que se ha introducido.
    return HttpResponse(status=302, headers={'Location': acortador.url})

def crearRegistro(url, short):
    # -- Ahora vamos a crear un nuevo registro en la tabla Acortadora y le vamos a pasar los valores de url y short.
    nuevoRegistro = Acortadora.objects.create(url=url, short=short)
    # -- Ahora vamos a guardar el objeto en la base de datos.
    nuevoRegistro.save()

def transformUrl(url):
    # -- Ahora vamos a transformar la url para que sea correcta.
    if not url.startswith('http://') or url.startswith('https://'):
        url = 'http://' + url
    return url

def correctUrl(url):
    # -- Ahora vamos a comprobar si la url es correcta.
    if url.startswith('http://') or url.startswith('https://'):
        return url
    else:
        url = transformUrl(url)
        return url

@csrf_exempt
def shortenUrl(request): # -- Cuando el recurso sea / se ejecutara esta funcion.
    if request.method == 'POST':
        if ('url' in request.POST) and ('short' in request.POST):
            short = request.POST['short']

            # -- comprobamos si la url introducida es correcta.
            url = correctUrl(request.POST['url'])

            # -- Ahora vamos a comprobar si la url ya esta en la base de datos y en caso contrario creamos el registro.
            try:
                Acortadora.objects.get(url=url)

            except Acortadora.DoesNotExist:
                # -- Ahora vamos a comprobar si el short esta vacio.
                if short == '':
                    # -- Primero consultamos el valor máximo del contador en la base de datos
                    contador = Acortadora.objects.all().aggregate(Max('contador'))['contador__max']
                    # -- Si el contador es None, es decir, no hay registros en la base de datos, entonces
                    # -- el contador será 0.
                    if contador is None:
                        contador = 0
                    # -- El short asignado al nuevo registro será igual al contador incrementado en 1.
                    short = contador + 1
                    contador += 1
                    actualizar_contador = Acortadora(contador=contador)
                    actualizar_contador.save()
                # -- Ahora vamos a crear el registro en la base de datos. Con el short introducido por el usuario
                #  (si lo ha introducido en el formulario) o con el contador incrementado en 1.
                crearRegistro(url, short)

            # -- Ahora vamos a renderizar la pagina con los datos.
            return render(request, 'acorta/post.html', {'registros': Acortadora.objects.values("url", "short")})
        else:
            # -- Si no se ha introducido una url o un short entonces, se manda un codigo 422.
            return HttpResponse("Contenido de la respuesta", status=422)

    elif request.method != 'GET':
        # -- Si es un metodo diferente de Get y Post entonces se manda un codigo 405.
        return HttpResponse(status=405)
    else:
        # -- Si no es un metodo POST entonces es un metodo GET, mandamos la pagina con el titulo
        # -- de la pagina y el formulario, asi como las paginas que ya se han acortado anteriormente.
        # -- Ahora en el return vamos a introducir la plantilla que vamos a usar de templates

        # Vamos a meter una plantilla llamada principal.html en la carpeta templates
        return render(request, 'acorta/principal.html', {'registros': Acortadora.objects.values("url", "short")})



from django.db import models

# -- Aquí creamos los modelos de la base de datos.

# -- Creamos la clase acortadora que va a tener los campos de la base de datos.
# -- Como es una aplicacion que quiere que el usuario escriba una url y un short,
# -- vamos a crear dos campos de tipo CharField, uno para la url y otro para el short.
# -- Asi como un id que es el que se va a usar para identificar cada url y short, y este

class Acortadora(models.Model):
    id = models.AutoField(primary_key=True)
    contador = models.IntegerField(default=0)
    url = models.CharField(max_length=200)
    short = models.CharField(max_length=200)
    # -- El contador se usa para si el usuario no
    # -- pone un short entonces se le asigna uno automaticamente y se le suma 1 al contador


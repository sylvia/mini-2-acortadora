from django.urls import path
from . import views

urlpatterns = [
    path('', views.shortenUrl, name='acortar'),
    path('<str:short>/', views.redirigir, name='redirigir'),
]